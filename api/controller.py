__author__ = 'ashwinpatti'


from api import api
from flask import jsonify,make_response,request,abort
from compute.prediction.categories.logisticRegression import predictOnListOfDict

@api.route('/')
@api.route('/index',methods=['GET'])
def index():
	return jsonify({'status':"Good"})


@api.route('/compute/prediction/categories/',methods=['POST'])
def PostCategories():
        if not request.get_json:
                abort(400)

        predictions=[]
        categoriesList = request.get_json(force=True)
        predictions = predictOnListOfDict(categoriesList,"compute/prediction/categories/models/model_01")
        return predictions

@api.errorhandler(404)
def not_found(error):
        return make_response(jsonify({'error':'Not found'}),404)

@api.errorhandler(400)
def invalid_data_format(error):
        return make_response(jsonify({'error':'Data is not JSON'}),400)

@api.errorhandler(405)
def not_found(error):
        return make_response(jsonify({'error':'Method not allowed'}),405)




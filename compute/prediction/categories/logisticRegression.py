
__author__ = 'venuktangirala'

import pandas as pd
from sklearn import linear_model as lm
from sklearn.externals import joblib
import numpy as np

def buildModel(dataFrame, modelStoreLocation):
    """
    builds a Generalised linear Model (GLM) logistic regression model and stores it to disk
    Parameters
    ----------
    dataFrame: a pandas data frame
    modelStoreLocation: location of the model on disk

    """
    # sort the dataframe by column names to make it consistent
    dataFrame = dataFrame.sort(axis=1,ascending=True)

    dataFrame = replaceNonNumeric(dataFrame)

    model = lm.LogisticRegression(penalty='l1')

    #  feature scaling before building the model
    # X = sklearn.preprocessing.scale(dataFrame.ix[:,:-1] ,with_mean=True,with_std=True)

    # selection all non category columns
    X = dataFrame.ix[:, ['ave_player_lifetime','ave_session_time','conversion_time',
                         'device','fb_login','monthly_spend','num_social_actions',
                         'total_session_time','total_spend']]

    # selection the column to predict
    y = dataFrame.ix[:, 'category']

    model.fit(X, y)

    joblib.dump(model, modelStoreLocation, compress=9)


def replaceNonNumeric(dataFrame):
    """
    replaces strings in the data to corresponding integer values

    Parameters
    ----------
    dataFrame: a pandas data frame

    Returns
    -------
    dataFrame: a pandas data frame
    """

    # replace fb_login with Yes=1 ; No=0
    dataFrame['fb_login'].replace("Yes", 1, inplace=True)
    dataFrame['fb_login'].replace("No", 0, inplace=True)

    # converting the fb_login datatype from object to int
    dataFrame['fb_login'] = dataFrame['fb_login'].astype(np.int64)

    # replace device type iOS=1 ;  Android =0
    dataFrame['device'].replace("iOS", 1, inplace=True)
    dataFrame['device'].replace("Android", 0, inplace=True)

    # converting the device datatype from object to int
    dataFrame['device'] = dataFrame['device'].astype(np.int64)

    return dataFrame


def predictOnDataFrame(dataToPredict, modelStoreLocation):
    """

    Parameters
    ----------
    modelStoreLocation : the location where the model is stored on disk
    dataToPredict : data you want to predict takes a numpy matrix or 9 columns

    Returns
    -------
    prediction: A list of predictions
    """

    #  loading the model stored on disk
    model = joblib.load(modelStoreLocation)

    # selecting all columns except category
    dataToPredict = dataToPredict.ix[:, ['ave_player_lifetime','ave_session_time','conversion_time',
                                         'device','fb_login','monthly_spend',
                                         'num_social_actions','total_session_time','total_spend']]

    # sort the dataframe by column names to make it consistent
    dataToPredict = dataToPredict.sort(axis=1,ascending =True)

    dataToPredict = replaceNonNumeric(dataToPredict)

    prediction = pd.DataFrame(model.predict_proba(dataToPredict) ,columns=model.classes_)

    return prediction

def predictOnListOfDict(listInput , modelStoreLocation):
    """

    Parameters
    ----------
    listInput: list of dict data to predict on
    modelStoreLocation: the model location

    Returns
    -------
    A json of predicted probabilities with their corresponding categories and the input values

    Here is an example return :
    [
        {
            "Dolphin": 0.363186833,
            "Free": 1.490557926e-21,
            "Minnow": 4.457519485e-30,
            "Whale": 0.636813167,
            "ave_player_lifetime": 117,
            "ave_session_time": 32,
            "conversion_time": 18,
            "device": "iOS",
            "fb_login": "Yes",
            "monthly_spend": 16.95,
            "num_social_actions": 21,
            "total_session_time": 224,
            "total_spend": 70.29
        },
        {
            "Dolphin": 0.2713884244,
            "Free": 6.944201994e-16,
            "Minnow": 6.808614637e-40,
            "Whale": 0.7286115756,
            "ave_player_lifetime": 108,
            "ave_session_time": 17,
            "conversion_time": 11,
            "device": "iOS",
            "fb_login": "Yes",
            "monthly_spend": 35.58,
            "num_social_actions": 19,
            "total_session_time": 176,
            "total_spend": 48.27
        }
    ]

    """
    dataToPredict = pd.DataFrame(listInput)

    predictionDataFrame = predictOnDataFrame(dataToPredict, modelStoreLocation)

    predictionDataFrame = 100* np.round(predictionDataFrame,4)

    predictionDataFrame = dataToPredict.join(predictionDataFrame)

    predictedJson = predictionDataFrame.to_json(orient='records')

    return predictedJson

if __name__ == '__main__':

    prediction =[]

    dataFrame = pd.read_csv('./data/nFlate_training_data.csv')

    modelStoreLocation = './models/model_01'

    # build model and store it at "modelStoreLocation"
    # buildModel(dataFrame, modelStoreLocation)

    # prediction = predictOnDataFrame(dataFrame.ix[1110:1120, :], modelStoreLocation)

    dictInput = [
                {
                    "ave_player_lifetime": 117, "ave_session_time": 32, "conversion_time": 18, 
                 "device": "iOS", "fb_login": "Yes", "monthly_spend": 16.95, 
                 "num_social_actions": 21, "total_session_time": 224, "total_spend": 70.29 
                }
                ,
                {
                    "ave_player_lifetime": 108, "ave_session_time": 17, "conversion_time": 11,
                 "device": "iOS", "fb_login": "Yes", "monthly_spend": 35.58,
                 "num_social_actions": 19, "total_session_time": 176, "total_spend": 48.27
                }
                ]

    prediction = predictOnListOfDict(dictInput,modelStoreLocation)

    print( "\npredictions :\n" + str(prediction) )

